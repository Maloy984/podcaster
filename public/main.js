Array.from(document.querySelectorAll(".header-navigation__link")).forEach(
  (link) => {
    link.addEventListener("click", (event) => {
      event.preventDefault();

      return window.scrollTo({
        top: document.querySelector(link.getAttribute("href")).offsetTop,
        behavior: "smooth",
      });
    });
  }
);
